from PIL import Image
import math

def resize_image(size, filename):
    """
    Resizes the image to the given size, but keeps the ratio of the original file, fills the rest with transparent.
    Saves the new image as a PNG with '_new' suffix.
    
    Args:
        size (tuple): (desired width, desired height)
        filename (string): path to file
    """
    im = Image.open(filename)
    w, h = im.width, im.height

    if w / h < size[0] / size[1]:
        ratio = size[1] / h
        resized_image = im.resize((math.floor(w*ratio), size[1]))
    else:
        ratio = size[0] / w
        resized_image = im.resize((size[0], math.floor(h*ratio)))

    margin_left = (size[0] - resized_image.width) // 2
    margin_top = (size[1] - resized_image.height) // 2

    new_image = Image.new('RGBA', (size[0], size[1]), (255, 0, 0, 0))
    new_image.paste(resized_image, (margin_left, margin_top))
    new_image.save(f'{filename}_new.png', 'PNG')
