Resizes the image to the given size, but keeps the ratio of the original. Fills the rest with transparent.
Saves the new image as a PNG with '_new' suffix.
